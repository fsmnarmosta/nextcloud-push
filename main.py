import gi
import json
import subprocess
import os
import threading

gi.require_version("Gtk", "3.0")
gi.require_version("Notify", "0.7")
from gi.repository import Gtk, GLib, Notify
from gi.repository.GdkPixbuf import Pixbuf

def recursive_enumerate(path):
    if not os.path.isdir(path):
        yield path
    else:
        yield from [x for x in os.scandir(path) if not os.path.isdir(x)]


def upload_clicked(url, root_path, icons, delete, upload_button):
    print(f"nextcloud url: {url}, files to upload: {root_path}")
    upload_button.set_sensitive(False)
    notification = Notify.Notification(summary="Synchronizing...")
    notification.set_image_from_pixbuf(icons["network-transmit"])
    notification.show()
    paths = list(recursive_enumerate(root_path))
    is_ok = True
    for i, path in enumerate(paths):
        notification.close()
        notification = Notify.Notification(summary="Synchronizing...", body=f"File {i+1} / {len(paths)}")
        notification.set_image_from_pixbuf(icons["network-transmit"])
        notification.show()
        if subprocess.run(["./cloudsend.sh", path, url]).returncode != 0:
            is_ok = False
            break
    notification.close()
    if is_ok:
        if delete:
            for path in paths:
                if not os.path.isdir(path):
                    os.remove(path)
        notification = Notify.Notification(summary="Successfully uploaded files")
        notification.set_image_from_pixbuf(icons["folder-remote"])
    else:
        notification = Notify.Notification(summary="Failed to upload files")
        notification.set_image_from_pixbuf(icons["network-error"])
    notification.show()
    upload_button.set_sensitive(True)

class NextcloudPushWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Nextcloud Push")
        self.set_border_width(10)
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        label = Gtk.Label(label="Nextcloud drop folder url")
        vbox.pack_start(label, False, True, 0)
        self.url_entry = Gtk.Entry()
        vbox.pack_start(self.url_entry, False, True, 0)
        label = Gtk.Label(label="Local path to drop")
        vbox.pack_start(label, False, True, 0)
        self.path_entry = Gtk.Entry()
        vbox.pack_start(self.path_entry, False, True, 0)
        self.post_delete = Gtk.CheckButton(label="Delete files after successful upload")
        vbox.pack_start(self.post_delete, False, True, 0)
        self.upload_button = Gtk.Button.new_with_label("Upload")
        self.upload_button.connect("clicked", lambda _: threading.Thread(target=(lambda: upload_clicked(self.url_entry.get_text(), self.path_entry.get_text(), self.icons, self.post_delete.get_active(), self.upload_button))).start())
        vbox.pack_start(self.upload_button, False, True, 0)
        self.add(vbox)

        try:
            with open("state", "r") as f:
                state = json.loads(f.read())
                self.url_entry.set_text(state["url"])
                self.path_entry.set_text(state["path"])
                self.post_delete.set_active(state["post_delete"])
        except IOError:
            pass

        self.url_entry.connect("changed", self.save_url_path)
        self.path_entry.connect("changed", self.save_url_path)
        self.post_delete.connect("toggled", self.save_url_path)
        self.icons = {}
        for icon in ["network-transmit", "network-error", "folder-remote"]:
            self.icons[icon] = Gtk.IconTheme.get_default().load_icon(icon, 64, 0)


    def save_url_path(self, entry):
        with open("state", "w+") as f:
            f.write(json.dumps({"url": self.url_entry.get_text(), "path": self.path_entry.get_text(), "post_delete": self.post_delete.get_active()}))

# GObject.threads_init() # init threads?

Notify.init("Nextcloud push")
win = NextcloudPushWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
